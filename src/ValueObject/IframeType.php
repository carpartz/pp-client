<?php
declare(strict_types=1);

namespace Ufo\Component\PartnerProgramClient\ValueObject;

final class IframeType extends AbstractValueObject
{
    public const CONVERSION = 1;
    public const DEPOSIT = 2;

    /**
     * @return int
     */
    public function getValue()
    {
        return (int) $this->value;
    }

    protected function isValidValue($value): bool
    {
        return in_array($value, [
            self::CONVERSION,
            self::DEPOSIT,
        ]);
    }
}
