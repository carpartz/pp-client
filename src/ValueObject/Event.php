<?php
declare(strict_types=1);

namespace Ufo\Component\PartnerProgramClient\ValueObject;

final class Event extends AbstractValueObject
{
    public const CLICK = 'click';
    public const SIGNUP = 'signup';
    public const DEPOSIT = 'deposit';

    protected function isValidValue($value): bool
    {
        return in_array($value, [
            self::CLICK,
            self::SIGNUP,
            self::DEPOSIT,
        ]);
    }
}
