<?php
declare(strict_types=1);

namespace Ufo\Component\PartnerProgramClient\ValueObject;

interface ValueObjectInterface
{
    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param string $value
     * @return self
     */
    public static function instance(string $value);

    public function equals(ValueObjectInterface $object): bool;

    public function __toString(): string;
}
