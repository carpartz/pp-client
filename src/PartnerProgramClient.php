<?php
declare(strict_types=1);

namespace Ufo\Component\PartnerProgramClient;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Ufo\Component\PartnerProgramClient\Entity\ModelToUpdate;
use Ufo\Component\PartnerProgramClient\Entity\Url;
use Ufo\Component\PartnerProgramClient\Exception\PartnerProgramClientException;
use Ufo\Component\PartnerProgramClient\ValueObject\Event;

final class PartnerProgramClient
{
    private string $baseUrl;
    private string $token;

    private ClientInterface $httpClient;

    public function __construct(string $baseUrl, string $token, ClientInterface $httpClient)
    {
        $this->baseUrl = $baseUrl;
        $this->token = $token;
        $this->httpClient = $httpClient;
    }

    public function getUrlById(int $urlId): Url
    {
        $data = $this->request('get', sprintf('url?id=%d', $urlId));
        if (!isset($data['url']) || !$data['url']) {
            throw new PartnerProgramClientException($data['message'] ?? 'Url not found');
        }

        return new Url($data['url']['smart_link'], $data['url']['iframe_conversion'], $data['url']['iframe_lead']);
    }

    /**
     * @param array $requestData
     * @return array
     * @throws GuzzleException
     * @throws PartnerProgramClientException
     */
    public function createAffiliate(array $requestData): array
    {
        $data = $this->request('post', 'affiliate', $requestData);
        if (!isset($data) || !$data) {
            throw new PartnerProgramClientException($data['message'] ?? 'Unable to create affiliate');
        }

        return $data['data'];
    }

    public function logEvent(Event $event, array $requestData): array
    {
        $requestData['event_name'] = $event->getValue();

        $data = $this->request('post', 'event', $requestData);
        if (!isset($data['url']) || !$data['url']) {
            throw new PartnerProgramClientException($data['message'] ?? 'Unable to log event');
        }

        return $data;
    }

    /**
     * @param ModelToUpdate[] $requestData
     * @param bool $respectProject
     * @return array
     * @throws GuzzleException
     * @throws PartnerProgramClientException
     */
    public function updateData(iterable $requestData, bool $respectProject = true): array
    {
        $items = [];
        foreach ($requestData as $rd) {
            $items[] = $rd->getBundle();
        }
        $data = $this->request('put', 'affiliate', [
            'respect_project' => $respectProject,
            'items' => $items,
        ]);
        if (!isset($data) || !$data) {
            throw new PartnerProgramClientException($data['message'] ?? 'Unable to create affiliate');
        }

        return $data['data'];
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $requestData
     * @return array
     * @throws PartnerProgramClientException
     * @throws GuzzleException
     */
    private function request(string $method, string $uri, array $requestData = []): array
    {
        $url = sprintf('%s/api/v1/%s', $this->baseUrl, $uri);
        $options = [
            'headers' => [
                'Authorization' => $this->getToken(),
                'Content-Type' => 'application/json',
            ],
        ];
        $options['json'] = $requestData;

        try {
            $response = $this->httpClient->request($method, $url, $options);

            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            throw new PartnerProgramClientException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    private function getToken(): string
    {
        return sprintf('Bearer %s', $this->token);
    }
}
