<?php
declare(strict_types=1);

namespace Ufo\Component\PartnerProgramClient\Exception;

class PartnerProgramClientException extends \Exception
{
}
