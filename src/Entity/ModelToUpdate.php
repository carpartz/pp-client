<?php
declare(strict_types=1);

namespace Ufo\Component\PartnerProgramClient\Entity;

class ModelToUpdate
{
    private string $modelName;
    private array $criteria = [];
    private array $data = [];

    public function __construct(string $modelName)
    {
        $this->modelName = $modelName;
    }

    /**
     * @param string $fieldName
     * @param mixed  $fieldValue
     */
    public function addCriteria(string $fieldName, $fieldValue): void
    {
        $this->criteria[$fieldName] = $fieldValue;
    }

    /**
     * @param string $fieldName
     * @param mixed  $fieldValue
     */
    public function addData(string $fieldName, $fieldValue): void
    {
        $this->data[$fieldName] = $fieldValue;
    }

    public function getBundle(): array
    {
        return [
            'model_name' => $this->modelName,
            'criteria' => $this->criteria,
            'data' => $this->data,
        ];
    }
}
