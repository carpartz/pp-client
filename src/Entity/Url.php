<?php
declare(strict_types=1);

namespace Ufo\Component\PartnerProgramClient\Entity;

class Url
{
    private string $smartLink;
    private ?string $iframeConversion;
    private ?string $iframeLead;

    public function __construct(string $smartLink, ?string $iframeConversion, ?string $iframeLead)
    {
        $this->smartLink = $smartLink;
        $this->iframeConversion = $iframeConversion;
        $this->iframeLead = $iframeLead;
    }

    public function getSmartLink(): string
    {
        return $this->smartLink;
    }

    public function getIframeConversion(): ?string
    {
        return $this->iframeConversion;
    }

    public function getIframeLead(): ?string
    {
        return $this->iframeLead;
    }
}
